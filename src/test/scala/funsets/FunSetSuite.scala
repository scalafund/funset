package funsets

/**
 * This class is a test suite for the methods in object FunSets.
 *
 * To run this test suite, start "sbt" then run the "test" command.
 */
class FunSetSuite extends munit.FunSuite:

  import FunSets.*

  test("contains is implemented") {
    assert(contains(x => true, 100))
  }

  /**
   * When writing tests, one would often like to re-use certain values for multiple
   * tests. For instance, we would like to create an Int-set and have multiple test
   * about it.
   *
   * Instead of copy-pasting the code for creating the set into every test, we can
   * store it in the test class using a val:
   *
   *   val s1 = singletonSet(1)
   *
   * However, what happens if the method "singletonSet" has a bug and crashes? Then
   * the test methods are not even executed, because creating an instance of the
   * test class fails!
   *
   * Therefore, we put the shared values into a separate trait (traits are like
   * abstract classes), and create an instance inside each test method.
   *
   */

  trait TestSets:
    val s1 = singletonSet(1)
    val s2 = singletonSet(2)
    val s3 = singletonSet(3)
    val s4 = union(s1 , s2)  // 1 ,2
    val s5 = union(union(s1 , s2), s3)  // 1 ,2 ,3
  /**
   * This test is currently disabled (by using .ignore) because the method
   * "singletonSet" is not yet implemented and the test would fail.
   *
   * Once you finish your implementation of "singletonSet", remove the
   * .ignore annotation.
   */

    test("singleton set one contains one") {
    
    /**
     * We create a new instance of the "TestSets" trait, this gives us access
     * to the values "s1" to "s3".
     */
    new TestSets:
      /**
       * The string argument of "assert" is a message that is printed in case
       * the test fails. This helps identifying which assertion failed.
       */
      assert(contains(s1, 1), "Singleton")
  }

  test("union contains all elements of each set") {
    new TestSets:
      val s = union(s1, s2)
      assert(contains(s, 1), "Union 1 i 2 check 1 ")
      assert(contains(s, 2), "Union 1 i 2 check 2")
      assert(!contains(s, 3), "Union 1 i 2 check 3")
  }

  test("intersect contains the set of all elements that are both sets") {
    new TestSets:
      val s = intersect(s1, s2)
      assert(!contains(s, 1), "Union 1 i 2 check 1 ")
      assert(!contains(s, 2), "Union 1 i 2 check 2")
      assert(!contains(s, 3), "Union 1 i 2 check 3")
  }
    test("intersect contains the set of all elements that are both sets"){
    new TestSets:
      val ss = intersect(s1, s5 )
      assert(contains(ss, 1), "intersect 1 i 5 check 1 ")
      assert(!contains(ss, 2), "intersect 1 i 2 check 2")
  }

  test("test diff") {
    new TestSets {
      val s = diff(s5, s1)
      assert(!contains(s, 1), "diff s5 i s1 ch 1")
      assert(contains(s, 2), "diff s5 i s1 ch 2")
    }
  }

  test("test filter") {
    new TestSets {
      val condition = (x: Int) => x > 2
      val result = filter(s5, condition)
      assert(!contains(result, 1), "filter s5  x > 2 ch 1")
      assert(!contains(result, 2), "filter s5  x > 2 ch 2")
      assert(contains(result, 3), "filter s5  x > 2  ch 3")
    }
  }

  test("test forall"){
    new TestSets {
      val s = (x: Int) => -1000 <= x && x <= 1000
      assert(!forall(s ,  (x: Int) => x > 50))
    }
  }

  test("test exist") {
    new TestSets {
      val s = (x: Int) => -1000 <= x && x <= 1000
      assert(exists(s, (x: Int) => x == 50))
    }
  }

  test("test map"){
    new TestSets {
      val set1: FunSet = (x:Int) => List[Int](1,2,3,4,5,6,7,8,9) contains x
      val resultSet: FunSet = map(set1, (x:Int) => x*4)
      assert(!contains(resultSet, 1))
      assert(!contains(resultSet, 3))
      assert(!contains(resultSet, 6))
      assert(contains(resultSet, 4))
      assert(contains(resultSet, 8))
      assert(contains(resultSet, 36))
    }
  }

  import scala.concurrent.duration.*
  override val munitTimeout = 10.seconds
